<?php
global $db;
error_reporting(0);
require 'config.php';

//echo file_get_contents('https://api.telegram.org/bot' . API_KEY . '/setwebhook?url=' . $_SERVER["SERVER_NAME"] . '' . $_SERVER["SCRIPT_NAME"] . '&allowed_updates=["message","edited_message","inline_query","query","callback_query","my_chat_member","chat_member","chosen_inline_result"]');

class Telegram
{

    private const API_KEY = "";

    public function cURL($method, array $datas = [])
    {
        $url = "https://api.telegram.org/bot" . self::API_KEY . "/" . $method;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $datas);
        $res = curl_exec($ch);
        return json_decode($res);
    }

    public function sendMessage($chat_id, $text, $keyboard = NULL)
    {
        return $this->cURL(
            "sendMessage",
            [
                'chat_id' => $chat_id,
                'text' => $text,
                'reply_markup' => $keyboard
            ]
        );
    }

    public function sendPhoto($chat_id, $photo, $caption, $keyboard = NULL)
    {
        return $this->cURL(
            "sendPhoto",
            [
                'chat_id' => $chat_id,
                'photo' => $photo,
                'caption' => $caption,
                'reply_markup' => $keyboard
            ]
        );
    }

    public function deleteMessage($chat_id, $message_id)
    {
        return $this->cURL(
            "deleteMessage",
            [
                'chat_id' => $chat_id,
                'message_id' => $message_id
            ]
        );
    }

    public function editMessage($chat_id, $message_id, $text)
    {
        return $this->cURL(
            "editMessageText",
            [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => $text
            ]
        );
    }

}

function getPosition($position_id)
{
    global $db;
    $sql = "SELECT name FROM position WHERE id = '$position_id'";
    $result = mysqli_query($db, $sql);
    if (mysqli_num_rows($result) > 0) {
        $row = mysqli_fetch_assoc($result);
        $position_name = $row['name'];
    } else {
        $position_name = "Lavozim topilmadi!";
    }
    return $position_name;
}

function step($chat_id, $value)
{
    return file_put_contents("step/$chat_id.txt", $value);
}

function clear_step($chat_id)
{
    return unlink("step/$chat_id.txt");
}

$bot = new Telegram();
$panel = json_encode(['resize_keyboard' => true,
    'keyboard' => [
        [['text' => "👤 Lavozimlar"], ['text' => "👥 Xodimlar"],],
        [['text' => "➕ Lavozim qo'shish"], ['text' => "➕ Xodim qo'shish"],],
        [['text' => "➖ Lavozim o'chirish"], ['text' => "➖ Xodim o'chirish"],],
        [['text' => ""],],
    ]
]);
$back = json_encode(['resize_keyboard' => true,
    'keyboard' => [
        [['text' => "🔙 Orqaga"],],
    ]
]);

$update = json_decode(file_get_contents("php://input"));
if ($update->callback_query) {

    $query_data = $update->callback_query->data;
    $message = $update->callback_query->message;
    $chat_id = $update->callback_query->message->chat->id;
} else {

    $message = $update->message;
    $message_id = $update->message->message_id;
    $chat_id = $message->chat->id;
    $text = $message->text;
    $step = file_get_contents("step/$chat_id.txt");

    if (isset($message->photo)) {
        $photo = $message->photo;
        $last_index = count($photo) - 1;
        $file_id = $photo[$last_index]->file_id; // oxirgi kelgan rasm tiniq bo'ladi
    }


    if ($chat_id == ADMIN) {

        if ($text == "/start" || $text == "🔙 Orqaga") {
            $bot->sendMessage($chat_id, "Quyidagi tugmalardan birini tanlang:", $panel);
            clear_step($chat_id);
            exit();
        }

        if ($text == "👥 Xodimlar") {
            $sql = "SELECT * FROM staff";
            $result = mysqli_query($db, $sql);

            $txt = "";

            if (mysqli_num_rows($result) > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $position_name = getPosition($row["position_id"]);
                    $status = $row['status'];
                    if ($status) {
                        $txt .= "✅ID: " . $row["id"] . " - FI: " . $row["name"] . " - " . $row["birthday"] . " - $position_name - /off_" . $row['id'] . "\n\n";
                    } else {
                        $txt .= "⛔️ID: " . $row["id"] . " - FI: " . $row["name"] . " - " . $row["birthday"] . " - $position_name - /on_" . $row['id'] . "\n\n";
                    }
                }
            } else {
                $txt .= "Xodimlar mavjud emas!";
                $bot->sendMessage($chat_id, $txt);
                exit();
            }

            mysqli_close($db);
            $bot->sendMessage($chat_id, $txt, $panel);
        }
        if ($text == "👤 Lavozimlar") {

            $sql = "SELECT * FROM position";
            $result = mysqli_query($db, $sql);
            $txt = "";
//            $bot->sendMessage($chat_id,json_encode(mysqli_num_rows($result)));
            if (mysqli_num_rows($result) > 0) {
                $i = 1;
                while ($row = mysqli_fetch_assoc($result)) {
                    $txt .= $i . ". " . $row["name"] . ", ID: " . $row["id"] . "\n";
                    $i++;
                }
            } else {
                $txt .= "Lavozimlar mavjud emas!";
                $bot->sendMessage($chat_id, $txt);
                exit();
            }

            mysqli_close($db);
            $bot->sendMessage($chat_id, $txt);
        }

        if ($text == "➕ Xodim qo'shish") {
            $bot->sendMessage($chat_id, "Xodim rasmini yuboring", $back);
            step($chat_id, "photo");
        }

        if ($step == "photo" && isset($message->photo)) {
            $bot->sendPhoto($chat_id, $file_id, "Saqlandi, endi xodim ma'lumotlarini quyidagi tartibda qatorma-qator yuboring:\n\n
            FIO
            tug'ilgan kun (YYYY-MM-DD)
            lavozim id
            ", $back);
            file_put_contents("temp/photo.txt", $file_id);
            step($chat_id, "info");
        }

        if ($step == "info") {
            $info = explode("\n", $text);
            if (count($info) < 3) {
                $bot->sendMessage($chat_id, "⚠️ Xatolik, iltimos xodim ma'lumotlarini quyidagi tartibda qatorma-qator yuboring:\n\nFIO\ntug'ilgan kun (YYYY-MM-DD)\nlavozim id");
            } else {
                $name = mysqli_real_escape_string($db, $info[0]);
                $b_day = mysqli_real_escape_string($db, $info[1]);
                $position_id = intval($info[2]);
                $file_id = file_get_contents("temp/photo.txt");
                $sql = "INSERT INTO staff (name, photo, birthday, position_id) VALUES ('$name', '$file_id', '$b_day', $position_id)";
                mysqli_query($db, $sql);
                $position_name = getPosition($position_id);
                $bot->sendPhoto($chat_id, $file_id, "Qo'shildi\n\nFIO: $name\nTug'ilgan kun: $b_day\nLavozim: $position_name", $panel);
                unlink("temp/photo.txt");
                clear_step($chat_id);
            }
        }

        if ($text == "➕ Lavozim qo'shish") {
            $bot->sendMessage($chat_id, "Lavozim nomini yuboring", $back);
            step($chat_id, "position");
        }

        if ($step == "position") {
            $name = mysqli_real_escape_string($text);
            $sql = "INSERT INTO position (name) VALUES ('$text')";
            mysqli_query($db, $sql);
            $bot->sendMessage($chat_id, "$text lavozimi muaffaqiyatli qo'shildi!", $panel);
            clear_step($chat_id);
        }

        if (mb_stripos($text, "/on_") !== false) {
            $ex = explode("_", $text);
            $id = intval($ex[1]);
            $sql = "UPDATE staff SET status = 1 WHERE id = $id";
            mysqli_query($db, $sql);
            $sql = "SELECT * FROM staff";
            $result = mysqli_query($db, $sql);
            $txt = "";
            if (mysqli_num_rows($result) > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $position_name = getPosition($row["position_id"]);
                    $status = $row['status'];
                    if ($status) {
                        $txt .= "✅ID: " . $row["id"] . " - FI: " . $row["name"] . " - " . $row["birthday"] . " - $position_name - /off_" . $row['id'] . "\n\n";
                    } else {
                        $txt .= "⛔️ID: " . $row["id"] . " - FI: " . $row["name"] . " - " . $row["birthday"] . " - $position_name - /on_" . $row['id'] . "\n\n";
                    }
                }
            }
            $bot->deleteMessage($chat_id, $message_id);
            $bot->deleteMessage($chat_id, $message_id - 1);
            $bot->sendMessage($chat_id, $txt, $panel);
        }
        if (mb_stripos($text, "/off_") !== false) {
            $ex = explode("_", $text);
            $id = intval($ex[1]);
            $sql = "UPDATE staff SET status = 0 WHERE id = $id";
            mysqli_query($db, $sql);
            $sql = "SELECT * FROM staff";
            $result = mysqli_query($db, $sql);
            $txt = "";
            if (mysqli_num_rows($result) > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $position_name = getPosition($row["position_id"]);
                    $status = $row['status'];
                    if ($status) {
                        $txt .= "✅ID: " . $row["id"] . " - FI: " . $row["name"] . " - " . $row["birthday"] . " - $position_name - /off_" . $row['id'] . "\n\n";
                    } else {
                        $txt .= "⛔️ID: " . $row["id"] . " - FI: " . $row["name"] . " - " . $row["birthday"] . " - $position_name - /on_" . $row['id'] . "\n\n";
                    }
                }
            }
            $bot->deleteMessage($chat_id, $message_id);
            $bot->deleteMessage($chat_id, $message_id - 1);
            $bot->sendMessage($chat_id, $txt, $panel);
        }
        if (mb_stripos($step, "tg_id_") !== false) {
            $ex = explode('_', $step);
            $staff_id = $ex[2];
            if (strlen($staff_id) > 5) {
                $update_query = "UPDATE staff SET tg_id = '$text' WHERE id = $staff_id";
                mysqli_query($db, $update_query);
                $bot->sendMessage($chat_id, "Saqlandi!", $panel);
                clear_step($chat_id);
            }
        }
    }
}
