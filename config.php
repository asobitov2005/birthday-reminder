<?php
const ADMIN = 00000; // telgram id
const HOST = "";
const USER = "";
const PASSWORD = "";
const DATABASE = "";

$db = mysqli_connect(HOST, USER, PASSWORD, DATABASE);
if (!$db) {
    file_get_contents("Ulanishda xatolik: " . mysqli_connect_error());
}

mysqli_query($db, "SET NAMES 'utf8'");

//mkdir("step");
//mkdir("temp");

$sql = "CREATE TABLE IF NOT EXISTS staff (
    id INT AUTO_INCREMENT PRIMARY KEY,
    tg_id INT DEFAULT FALSE,
    name VARCHAR(255),
    photo VARCHAR(255),
    birthday DATE,
    position_id INT,
    status BOOLEAN DEFAULT FALSE
)";

if (mysqli_query($db, $sql)) {
    echo "User table yaratildi! <br>";
} else {
    echo "User table yaratishda xatolik: " . mysqli_error($db) . "<br>";
}

$sql = "CREATE TABLE IF NOT EXISTS position (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255)
)";

if (mysqli_query($db, $sql)) {
    echo "Position table yaratildi!<br>";
} else {
    echo "Position table yaratishda xatolik: " . mysqli_error($db) . "<br>";
}





